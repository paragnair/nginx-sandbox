# local.nginx.com

server {
    listen 80 default_server;
    server_name local.nginx.com;

    # Root document
    root /usr/share/nginx/html;
    index index.html index.htm;

    # add X-Cache-Status header
    add_header X-Cache-Status $upstream_cache_status always;

    # Redirects
    location /redirecting.html {
        return 301 $scheme://$http_host/redirected.html;
    }
}