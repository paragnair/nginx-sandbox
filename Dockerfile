FROM nginx

# Copy the config file
COPY nginx.conf /etc/nginx/nginx.conf

# Copy sites enabled
COPY sites-enabled/ /etc/nginx/sites-enabled/